package com.kevinroese.quartiermeister.services

import com.kevinroese.quartiermeister.core.FullCampaignDTO
import com.kevinroese.quartiermeister.core.SimpleCampaignDTO
import com.kevinroese.quartiermeister.core.InitialDTO
import com.kevinroese.quartiermeister.core.RankDTO
import com.kevinroese.quartiermeister.mariadb.Campaign
import com.kevinroese.quartiermeister.mariadb.repositories.*
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class CampaignService(
    val campaignRepository: CampaignRepository,
    val rankRepository: RankRepository,
    val transactionRepository: TransactionRepository,
    val monthlyTransactionRepository: MonthlyTransactionRepository,
    val commonTransactionRepository: CommonTransactionRepository,
    val shipRepository: ShipRepository,
    val commonItemRepository: CommonItemRepository,

    val transactionService: TransactionService,
    val monthlyTransactionService: MonthlyTransactionService,
    val commonTransactionService: CommonTransactionService,
    val shipService: ShipService,
    val commonItemService: CommonItemService
) {

    fun getInitial():InitialDTO {
        return InitialDTO(
            campaigns = campaignRepository.findAll().sortedBy { it.status }.map {
                SimpleCampaignDTO(it.id, it.name, it.status)
            },
            ranks = rankRepository.findAll().map {
                RankDTO(it.id, it.name, it.salary)
            },
            lastTransaction = transactionRepository.findMaxId()?: 0,
            lastMonthlyTransaction = monthlyTransactionRepository.findMaxId()?: 0,
            lastCommonTransaction = commonTransactionRepository.findMaxId()?: 0,
            lastShip = shipRepository.findMaxId()?: 0,
            lastCommonItem = commonItemRepository.findMaxId()?: 0
        )
    }

    fun getCampaign(campaignId: Long): SimpleCampaignDTO {
        return campaignRepository.findByIdOrNull(campaignId)?.let {
            SimpleCampaignDTO(it.id, it.name, it.status)
        }?: throw Exception("No campaign selected!")
    }

    fun getGold(campaignId: Long): Double {
        return campaignRepository.findByIdOrNull(campaignId)?.gold?: throw Exception("No campaign selected!")
    }

    fun getNotes(campaignId: Long): String {
        val campaign = campaignRepository.findByIdOrNull(campaignId)?: throw Exception("No campaign selected!")
        return campaign.notes?: ""
    }

    fun saveCampaign(campaign: FullCampaignDTO) {
        campaignRepository.save(
            Campaign(
                id = campaign.id,
                name = campaign.name,
                gold = campaign.gold,
                status = campaign.status,
                notes = campaign.notes
            )
        )
        for(transaction in campaign.transactions) {
            transactionService.saveTransaction(transaction)
        }
        for(transaction in campaign.transactions_executed) {
            transactionService.saveTransaction(transaction)
        }
        for(transaction in campaign.monthly_transactions) {
            monthlyTransactionService.saveMonthlyTransaction(transaction)
        }
        for(transaction in campaign.common_transactions) {
            commonTransactionService.saveTransaction(transaction)
        }
        for(ship in campaign.ships) {
            shipService.saveShip(ship)
        }
        for(item in campaign.common_items) {
            commonItemService.saveCommonItem(item)
        }
    }

    fun saveCampaignStatus(campaign: SimpleCampaignDTO) {
        campaignRepository.saveStatus(
            id = campaign.id,
            status = campaign.status
        )
    }
}