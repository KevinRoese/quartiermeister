package com.kevinroese.quartiermeister.mariadb

import jakarta.persistence.*

@Entity
@Table(name = "ships")
data class Ship(
    @Id
    val id: Long,
    val campaign: Long,
    val name: String,
    val active: Boolean = true
)
