package com.kevinroese.quartiermeister.mariadb.repositories

import com.kevinroese.quartiermeister.mariadb.Transaction
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface TransactionRepository: CrudRepository<Transaction, Long> {
    fun findAllByCampaignAndExecuted(campaign: Long, executed: Boolean): List<Transaction>
    fun findAllByParentTransaction(parentTransaction: Long?): List<Transaction>

    @Query(value = "SELECT MAX(id) FROM transactions", nativeQuery = true)
    fun findMaxId(): Long?
}