package com.kevinroese.quartiermeister.controllers

import com.kevinroese.quartiermeister.core.CommonTransactionDTO
import com.kevinroese.quartiermeister.core.MonthlyTransactionDTO
import com.kevinroese.quartiermeister.core.TransactionDTO
import com.kevinroese.quartiermeister.core.FullCampaignDTO
import com.kevinroese.quartiermeister.services.CommonTransactionService
import com.kevinroese.quartiermeister.services.MonthlyTransactionService
import com.kevinroese.quartiermeister.services.TransactionService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
@CrossOrigin(maxAge = 72000)
class TransactionController(
    val transactionService: TransactionService,
    val monthlyTransactionService: MonthlyTransactionService,
    val commonTransactionService: CommonTransactionService
) {
    @PostMapping("/transaction/saveSingle")
    fun saveTransaction(@RequestBody transaction: TransactionDTO) {
        transactionService.saveTransaction(transaction)
    }

    @PostMapping("/transaction/saveMonthly")
    fun saveMonthlyTransaction(@RequestBody transaction: MonthlyTransactionDTO) {
        monthlyTransactionService.saveMonthlyTransaction(transaction)
    }

    @PostMapping("/transaction/saveCommon")
    fun saveCommonTransaction(@RequestBody transaction: CommonTransactionDTO) {
        commonTransactionService.saveTransaction(transaction)
    }

    @GetMapping("/transaction/delete/{transactionId}")
    fun deleteTransaction(@PathVariable transactionId: Long) {
        transactionService.deleteTransaction(transactionId)
    }

    @GetMapping("/transaction/deleteMonthly/{transactionId}")
    fun deleteMonthlyTransaction(@PathVariable transactionId: Long) {
        monthlyTransactionService.deleteMonthlyTransaction(transactionId)
    }

    @GetMapping("/transaction/deleteCommon/{transactionId}")
    fun deleteCommonTransaction(@PathVariable transactionId: Long) {
        commonTransactionService.deleteTransaction(transactionId)
    }
}