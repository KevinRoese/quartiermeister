package com.kevinroese.quartiermeister.services

import com.kevinroese.quartiermeister.core.TransactionDTO
import com.kevinroese.quartiermeister.mariadb.Transaction
import com.kevinroese.quartiermeister.mariadb.repositories.TransactionRepository
import org.springframework.stereotype.Service

@Service
class TransactionService(
    val transactionRepository: TransactionRepository
) {
    fun listTransactions(campaignId: Long): List<TransactionDTO> {
        return transactionRepository.findAllByCampaignAndExecuted(campaignId, false)
            .filter { it.parentTransaction == null }
            .map {
            TransactionDTO(
                id = it.id,
                description = it.description?: "",
                amount = it.amount,
                multiplier = it.multiplier,
                executed = false,
                subtransactions = transactionRepository.findAllByParentTransaction(it.id).map { sub ->
                    TransactionDTO(
                        id = sub.id,
                        description = sub.description ?:"",
                        amount = sub.amount,
                        multiplier = sub.multiplier,
                        executed = false,
                        subtransactions = listOf()
                    )
                }
            )
        }
    }

    fun listTransactionsExecuted(campaignId: Long): List<TransactionDTO> {
        return transactionRepository.findAllByCampaignAndExecuted(campaignId, true)
            .filter { it.parentTransaction == null }
            .map {
            TransactionDTO(
                id = it.id,
                description = it.description?: "",
                amount = it.amount,
                multiplier = it.multiplier,
                executed = true,
                subtransactions = transactionRepository.findAllByParentTransaction(it.id).map { sub ->
                    TransactionDTO(
                        id = sub.id,
                        description = sub.description ?:"",
                        amount = sub.amount,
                        multiplier = sub.multiplier,
                        executed = true,
                        subtransactions = listOf()
                    )
                }
            )
        }
    }

    fun deleteTransaction(transactionId: Long) {
        transactionRepository.deleteById(transactionId)
    }

    fun saveTransaction(transaction: TransactionDTO) {
        transactionRepository.save(
            Transaction(
                id = transaction.id,
                description = transaction.description,
                amount = transaction.amount,
                multiplier = transaction.multiplier,
                parentTransaction = null,
                executed = transaction.executed,
                campaign = transaction.campaign?: throw Exception("No campaign selected!")
            )
        )
        for (subTransaction in transaction.subtransactions?: listOf()) {
            transactionRepository.save(
                Transaction(
                    id = subTransaction.id,
                    description = subTransaction.description,
                    amount = subTransaction.amount,
                    multiplier = subTransaction.multiplier,
                    parentTransaction = transaction.id,
                    executed = transaction.executed,
                    campaign = transaction.campaign
                )
            )
        }
    }
}