package com.kevinroese.quartiermeister.mariadb.repositories

import com.kevinroese.quartiermeister.mariadb.CommonTransaction
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface CommonTransactionRepository: CrudRepository<CommonTransaction, Long> {
    fun findAllByCampaign(campaign: Long): List<CommonTransaction>
    fun findAllByParentTransaction(parentTransaction: Long?): List<CommonTransaction>

    @Query(value = "SELECT MAX(id) FROM common_transactions", nativeQuery = true)
    fun findMaxId(): Long?
}