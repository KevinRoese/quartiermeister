export class MonthlyTransaction {
    id: number = 0;
    description: string = "";
    amount: number = 0.0;
    campaign: number = 0;
}