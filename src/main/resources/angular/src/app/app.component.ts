import { Component, HostListener, OnInit } from '@angular/core';
import { Transaction } from './shared/models/transaction';
import { Campaign } from './shared/models/campaign';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Initial } from './shared/models/initial';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { MonthlyTransaction } from './shared/models/monthlytransaction';
import { Rank } from './shared/models/rank';
import { Ship } from './shared/models/ship';
import { Crew } from './shared/models/crew';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { CommonTransaction } from './shared/models/commontransaction';
import { CommonItem } from './shared/models/commonitem';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'quartiermeister';

  campaign : Campaign = new Campaign;
  campaigns : Campaign[] = [];
  transactions : Transaction[] = [];
  transactions_executed : Transaction[] = [];
  monthly_transactions: MonthlyTransaction[] = [];
  common_transactions: CommonTransaction[] = [];
  ranks: Rank[] = [];
  ships: Ship[] = [];
  common_items: CommonItem[] = [];

  projectedGold: number = 0;
  lastTransaction: number = 0;
  lastMonthlyTransaction: number = 0;
  lastCommonTransaction: number = 0;
  lastShip: number = 0;
  lastCommonItem: number = 0;

  display_ships: boolean = false;

  snackbarOK = "OK";
  snackbarConfig: MatSnackBarConfig = {
    duration: 1500,
    horizontalPosition: 'start',
  }; 

  url = "http://localhost:8081/"
  jsonHeaders = new HttpHeaders({
    'Content-Type': 'application/json',
  });

  constructor(private http: HttpClient, private snackbar: MatSnackBar) {
  }

  @HostListener('window:beforeunload')
   beforeUnloadHandler() {
    //autosave on close
    //this.saveAll();
   }

  ngOnInit() {
    this.http.get(this.url + "initial").subscribe(
      (response) => {
        let initial = response as Initial;
        this.campaigns = initial.campaigns;
        this.changeCampaign(this.campaigns[0], true);
        this.ranks = initial.ranks;
        this.lastTransaction = initial.lastTransaction;
        this.lastMonthlyTransaction = initial.lastMonthlyTransaction;
        this.lastCommonTransaction = initial.lastCommonTransaction;
        this.lastShip = initial.lastShip;
        this.lastCommonItem = initial.lastCommonItem;
      },
      (error) => {
        console.error(error);
      },
      () => {
        
      }
    )
  }

  snack(message: string) {
    this.snackbar.open(message, this.snackbarOK, this.snackbarConfig);
  }

  changeCampaign(campaign: Campaign, init: boolean = false) {
    if(!init) {
      this.saveAll();
    }
    this.campaign = campaign;
    this.http.get(this.url + "campaign/" + campaign.id).subscribe(
      (response) => {
        let campaign = response as Campaign;
        this.campaign.gold = campaign.gold;
        this.campaign.notes = campaign.notes;
        this.transactions = campaign.transactions;
        this.transactions_executed = campaign.transactions_executed;
        this.monthly_transactions = campaign.monthly_transactions;
        this.common_transactions = campaign.common_transactions;
        this.ships = campaign.ships;
        this.common_items = campaign.common_items;
        for (let t of this.transactions) {
          t.campaign = campaign.id;
        }
        for (let t of this.transactions_executed) {
          t.campaign = campaign.id;
        }
        for (let t of this.monthly_transactions) {
          t.campaign = campaign.id;
        }
        for (let t of this.common_transactions) {
          t.campaign = campaign.id;
        }
        for (let s of this.ships) {
          s.campaign = campaign.id;
        }
        for (let i of this.common_items) {
          i.campaign = campaign.id;
        }
        this.display_ships = this.ships.length > 0;
        this.updateProjected();
      },
      (error) => {
        console.error(error);
      },
      () => {
        
      }
    )
  }

  setCampaignStatus(campaign: Campaign, status: number) {
    campaign.status = status;
    this.http.post(this.url + "campaign/saveStatus", campaign, { headers: this.jsonHeaders }).subscribe(
      (response) => {

      },
      (error) => {
        console.error(error);
      },
      () => {
        
      }
    )
  }

  addShip() {
    let ship = new Ship();
    this.lastShip++;
    ship.id = this.lastShip;
    ship.campaign = this.campaign.id;
    ship.crew = [];
    for(let r of this.ranks) {
      let crew = new Crew();
      crew.rank = r.id;
      crew.count = 0;
      ship.crew.push(crew);
    }
    this.ships.push(ship);
  }

  saveShip(ship: Ship) {
    this.http.post(this.url + "ships/saveSingle", ship, { headers: this.jsonHeaders }).subscribe(
      (response) => {
        this.snack("Schiff gespeichert!");
      },
      (error) => {
        console.error(error);
      },
      () => {
        
      }
    )
  }

  removeShip(ship: Ship) {
    this.ships.splice(this.ships.indexOf(ship), 1);
    this.http.get(this.url + "ships/delete/" + ship.id).subscribe(
      (response) => {
        
      },
      (error) => {
        console.error(error);
      },
      () => {
        
      }
    )
  }

  payShip(ship: Ship, event: Event) {
    let transaction = this.addTransaction();
    transaction.description = "Heuer " + ship.name;
    for (let r of this.ranks) {
      let subtransaction = this.addSubtransaction(transaction, event);
      subtransaction.amount = -r.salary;
      subtransaction.description = r.name;
      subtransaction.multiplier = ship.crew[r.id-1].count;
    }
    this.updateParent(transaction);
  }

  saveRanks(button: boolean = false) {
    this.http.post(this.url + "ranks/save", this.ranks, { headers: this.jsonHeaders }).subscribe(
      (response) => {
        if(button) {
          this.snack("Heuer gespeichert!");
        }
      },
      (error) => {
        console.error(error);
      },
      () => {
        
      }
    )
  }

  addCommonItem(): CommonItem {
    let item = new CommonItem();
    this.lastCommonItem++;
    item.id = this.lastCommonItem;
    item.campaign = this.campaign.id;
    this.common_items.push(item);
    return item;
  }

  removeCommonItem(item: CommonItem) {
     this.common_items.splice(this.common_items.indexOf(item), 1);
     this.http.get(this.url + "items/delete/" + item.id).subscribe(
       (response) => {
         
       },
       (error) => {
         console.error(error);
       },
       () => {
         
       }
     )
  }

  saveCommonItem(item: CommonItem, event: Event) {
    event.stopPropagation();
    this.http.post(this.url + "items/saveSingle", item, { headers: this.jsonHeaders }).subscribe(
      (response) => {
        this.snack("Speichern erfolgreich!");
      },
      (error) => {
        console.error(error);
      },
      () => {
        
      }
    )
  }

  addTransaction(): Transaction {
    let transaction = new Transaction();
    this.lastTransaction++;
    transaction.id = this.lastTransaction;
    transaction.campaign = this.campaign.id;
    this.transactions.push(transaction);
    return transaction;
  }

  removeTransaction(transaction: Transaction) {
     this.transactions.splice(this.transactions.indexOf(transaction), 1);
     this.http.get(this.url + "transaction/delete/" + transaction.id).subscribe(
       (response) => {
         
       },
       (error) => {
         console.error(error);
       },
       () => {
         
       }
     )
     this.updateProjected();
  }

  update() {
     this.updateProjected();
     this.saveAll();
  }

  saveAll(button: boolean = false) {
    this.saveRanks();
    let campaign: Campaign = new Campaign();
    campaign.id = this.campaign.id;
    campaign.name = this.campaign.name;
    campaign.gold = this.campaign.gold;
    campaign.notes = this.campaign.notes;
    campaign.transactions = this.transactions;
    campaign.transactions_executed = this.transactions_executed;
    campaign.monthly_transactions = this.monthly_transactions;
    campaign.common_transactions = this.common_transactions;
    campaign.ships = this.ships;
    campaign.common_items = this.common_items;
    //save campaign
    this.http.post(this.url + "campaign/save", campaign, { headers: this.jsonHeaders }).subscribe(
      (response) => {

      },
      (error) => {
        console.error(error);
      },
      () => {
        
      }
    )
    if(button) {
      this.snack("Kampagne gespeichert!");
    }
  }

  save(transaction: Transaction, event: Event) {
    event.stopPropagation();
    this.http.post(this.url + "transaction/saveSingle", transaction, { headers: this.jsonHeaders }).subscribe(
      (response) => {
        this.snack("Speichern erfolgreich!");
      },
      (error) => {
        console.error(error);
      },
      () => {
        
      }
    )
  }

  executeTransaction(transaction: Transaction, event: Event) {
    event.stopPropagation();
    if(transaction.amount < 0 && (this.campaign.gold < -this.round(transaction.multiplier*transaction.amount))) {
      this.snack("Nicht genug Gold!");
      return;
    }
    this.campaign.gold = this.round(this.campaign.gold + transaction.multiplier*transaction.amount);
    transaction.executed = true;
    for(let s of transaction.subtransactions) {
      s.executed = true;
    }
    this.transactions_executed.push(transaction);
    this.removeTransaction(transaction);
    this.update();
  }

  round(num: number): number {
    return Math.round(num*100)/100;
  }

  range(start: number, end: number): number[] {
    let range: number[] = [];
    for(let i = start; i < end; i++) {
      range.push(i);
    }
    return range;
  }

  drop(array: any[], event: CdkDragDrop<string[]>) {
    moveItemInArray(array, event.previousIndex, event.currentIndex);
    let id = array[event.previousIndex].id;
    array[event.previousIndex].id = array[event.currentIndex].id;
    array[event.currentIndex].id = id;
    this.saveAll();
  }

  updateProjected() {
    let sum = 0;
    for (let t of this.transactions) {
      sum += t.multiplier*t.amount;
    }
    this.projectedGold = this.round(this.campaign.gold + sum);
  }

  addSubtransaction(transaction: Transaction, event: Event): Transaction {
    transaction.expanded = true;
    event.stopPropagation();
    let subtransaction = new Transaction();
    this.lastTransaction++;
    subtransaction.id = this.lastTransaction;
    subtransaction.campaign = this.campaign.id;
    if(transaction.subtransactions.length == 0) {
      subtransaction.amount = transaction.amount;
      subtransaction.multiplier = transaction.multiplier;
    }
    transaction.subtransactions.push(subtransaction);
    return subtransaction;
  }

  removeSubtransaction(transaction: Transaction, subtransaction: Transaction) {
    transaction.subtransactions.splice(transaction.subtransactions.indexOf(subtransaction), 1);
    this.http.get(this.url + "transaction/delete/" + subtransaction.id).subscribe(
      (response) => {
        
      },
      (error) => {
        console.error(error);
      },
      () => {
        
      }
    )
    this.updateParent(transaction);
    if(transaction.subtransactions.length == 0) {
      transaction.amount = subtransaction.amount;
      transaction.multiplier = subtransaction.multiplier;
      transaction.expanded = false;
    }
  }

  updateParent(transaction: Transaction) {
    let sum = 0;
    for (let s of transaction.subtransactions) {
      sum += s.multiplier*s.amount;
    }
    transaction.amount = this.round(sum);
    this.updateProjected();
  }

  insertMonthlyTransactions(event: Event) {
    let transaction = this.addTransaction();
    transaction.description = "Monatliche Transaktionen";
    for (let m of this.monthly_transactions) {
      let subtransaction = this.addSubtransaction(transaction, event);
      subtransaction.amount = m.amount;
      subtransaction.description = m.description;
    }
    this.updateParent(transaction);
  }

  addMonthlyTransaction() {
    let transaction = new MonthlyTransaction();
    this.lastTransaction++;
    transaction.id = this.lastTransaction;
    transaction.campaign = this.campaign.id;
    this.monthly_transactions.push(transaction);
  }

  saveMonthlyTransaction(transaction: MonthlyTransaction) {
    this.http.post(this.url + "transaction/saveMonthly", transaction, { headers: this.jsonHeaders }).subscribe(
      (response) => {
        this.snack("Speichern erfolgreich!");
      },
      (error) => {
        console.error(error);
      },
      () => {
        
      }
    )
  }

  removeMonthlyTransaction(transaction: MonthlyTransaction) {
    this.monthly_transactions.splice(this.monthly_transactions.indexOf(transaction), 1);
    this.http.get(this.url + "transaction/deleteMonthly/" + transaction.id).subscribe(
      (response) => {
        
      },
      (error) => {
        console.error(error);
      },
      () => {
        
      }
    )
  }

  addCommonTransaction() {
    let transaction = new CommonTransaction();
    this.lastTransaction++;
    transaction.id = this.lastTransaction;
    transaction.campaign = this.campaign.id;
    this.common_transactions.push(transaction);
  }

  removeCommonTransaction(transaction: CommonTransaction) {
     this.common_transactions.splice(this.common_transactions.indexOf(transaction), 1);
     this.http.get(this.url + "transaction/deleteCommon/" + transaction.id).subscribe(
       (response) => {
         
       },
       (error) => {
         console.error(error);
       },
       () => {
         
       }
     )
     this.updateProjected();
  }

  saveCommon(transaction: CommonTransaction, event: Event) {
    event.stopPropagation();
    this.http.post(this.url + "transaction/saveCommon", transaction, { headers: this.jsonHeaders }).subscribe(
      (response) => {
        this.snack("Speichern erfolgreich!");
      },
      (error) => {
        console.error(error);
      },
      () => {
        
      }
    )
  }

  insertCommonTransaction(_transaction: CommonTransaction, event: Event) {
    event.stopPropagation();
    let transaction = this.addTransaction();
    transaction.description = _transaction.description;
    transaction.campaign = this.campaign.id;
    if(_transaction.subtransactions.length > 0) {
      for (let s of _transaction.subtransactions) {
        let subtransaction = this.addSubtransaction(transaction, event);
        subtransaction.amount = s.amount;
        subtransaction.description = s.description;
        subtransaction.campaign = this.campaign.id;
      }
      this.updateParent(transaction);
    } else {
      transaction.amount = _transaction.amount;
    }
  }

  addCommonSubtransaction(transaction: CommonTransaction, event: Event) {
    transaction.expanded = true;
    event.stopPropagation();
    let subtransaction = new CommonTransaction();
    this.lastTransaction++;
    subtransaction.id = this.lastTransaction;
    subtransaction.campaign = this.campaign.id;
    if(transaction.subtransactions.length == 0) {
      subtransaction.amount = transaction.amount;
    }
    transaction.subtransactions.push(subtransaction);
  }

  removeCommonSubtransaction(transaction: CommonTransaction, subtransaction: CommonTransaction) {
    transaction.subtransactions.splice(transaction.subtransactions.indexOf(subtransaction), 1);
    this.http.get(this.url + "transaction/deleteCommon/" + subtransaction.id).subscribe(
      (response) => {
        
      },
      (error) => {
        console.error(error);
      },
      () => {
        
      }
    )
    this.updateCommonParent(transaction);
    if(transaction.subtransactions.length == 0) {
      transaction.amount = subtransaction.amount;
      transaction.expanded = false;
    }
  }

  updateCommonParent(transaction: CommonTransaction) {
    let sum = 0;
    for (let s of transaction.subtransactions) {
      sum += s.amount;
    }
    transaction.amount = this.round(sum);
  }  
}
