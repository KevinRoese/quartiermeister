package com.kevinroese.quartiermeister.mariadb.repositories

import com.kevinroese.quartiermeister.mariadb.CommonItem
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface CommonItemRepository: CrudRepository<CommonItem, Long> {
    fun findAllByCampaign(campaign: Long): List<CommonItem>

    @Query(value = "SELECT MAX(id) FROM common_items", nativeQuery = true)
    fun findMaxId(): Long?
}