package com.kevinroese.quartiermeister.mariadb

import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table

@Entity
@Table(name = "ranks")
data class Rank(
    @Id
    val id: Long,
    val name: String,
    val salary: Double
)
