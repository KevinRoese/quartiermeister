package com.kevinroese.quartiermeister.controllers

import com.kevinroese.quartiermeister.core.FullCampaignDTO
import com.kevinroese.quartiermeister.core.SimpleCampaignDTO
import com.kevinroese.quartiermeister.core.InitialDTO
import com.kevinroese.quartiermeister.services.*
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin(maxAge = 72000)
class CampaignController(
    val campaignService: CampaignService,
    val transactionService: TransactionService,
    val monthlyTransactionService: MonthlyTransactionService,
    val commonTransactionService: CommonTransactionService,
    val shipService: ShipService,
    val commonItemService: CommonItemService
) {
    @GetMapping("/initial")
    fun getInitial(): InitialDTO {
        return campaignService.getInitial()
    }

    @PostMapping("/campaign/save")
    fun saveCampaign(@RequestBody campaign: FullCampaignDTO) {
        campaignService.saveCampaign(campaign)
    }

    @PostMapping("/campaign/saveStatus")
    fun saveCampaignStatus(@RequestBody campaign: SimpleCampaignDTO) {
        campaignService.saveCampaignStatus(campaign)
    }
    @GetMapping("/campaign/{campaignId}")
    fun getCampaign(@PathVariable campaignId: Long): FullCampaignDTO {
        val campaign = campaignService.getCampaign(campaignId)
        return FullCampaignDTO(
            id = campaignId,
            name = campaign.name,
            status = campaign.status,
            gold = campaignService.getGold(campaignId),
            notes = campaignService.getNotes(campaignId),
            transactions = transactionService.listTransactions(campaignId),
            transactions_executed = transactionService.listTransactionsExecuted(campaignId),
            monthly_transactions = monthlyTransactionService.listMonthlyTransactions(campaignId),
            common_transactions = commonTransactionService.listTransactions(campaignId),
            ships = shipService.listShips(campaignId),
            common_items = commonItemService.listCommonItems(campaignId)
        )
    }
}