package com.kevinroese.quartiermeister.mariadb

import jakarta.persistence.*

@Entity
@Table(name = "transactions")
data class Transaction(
    @Id
    val id: Long,
    val campaign: Long,
    val description: String? = null,
    @Column(name = "parent_transaction")
    val parentTransaction: Long? = null,
    val executed: Boolean = false,
    val amount: Double,
    val multiplier: Int
)
