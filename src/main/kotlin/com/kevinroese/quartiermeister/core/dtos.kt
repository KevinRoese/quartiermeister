package com.kevinroese.quartiermeister.core

data class SimpleCampaignDTO(
    val id: Long,
    val name: String,
    val status: Int
)

data class FullCampaignDTO(
    val id: Long,
    val name: String,
    val status: Int,
    val gold: Double,
    val notes: String,
    val transactions: List<TransactionDTO>,
    val transactions_executed: List<TransactionDTO>,
    val monthly_transactions: List<MonthlyTransactionDTO>,
    val common_transactions: List<CommonTransactionDTO>,
    val ships: List<ShipDTO>,
    val common_items: List<CommonItemDTO>
)

data class TransactionDTO(
    val id: Long,
    val description: String,
    val subtransactions: List<TransactionDTO>?,
    val amount: Double,
    val multiplier: Int,
    val executed: Boolean,
    val campaign: Long? = null
)

data class MonthlyTransactionDTO(
    val id: Long,
    val description: String,
    val amount: Double,
    val campaign: Long? = null
)

data class CommonTransactionDTO(
    val id: Long,
    val description: String,
    val subtransactions: List<CommonTransactionDTO>?,
    val amount: Double,
    val campaign: Long? = null
)

data class InitialDTO(
    val campaigns: List<SimpleCampaignDTO>,
    val ranks: List<RankDTO>,
    val lastTransaction: Long,
    val lastMonthlyTransaction: Long,
    val lastCommonTransaction: Long,
    val lastShip: Long,
    val lastCommonItem: Long
)

data class ShipDTO(
    val id: Long,
    val name: String,
    val active: Boolean,
    val crew: List<CrewDTO>,
    val campaign: Long? = null
)

data class CrewDTO(
    val rank: Long,
    val count: Int = 0
)

data class RankDTO(
    val id: Long,
    val name: String,
    val salary: Double
)

data class CommonItemDTO(
    val id: Long,
    val name: String,
    val amount: Int,
    val campaign: Long? = null
)
