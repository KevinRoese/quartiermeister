package com.kevinroese.quartiermeister.services

import com.kevinroese.quartiermeister.core.MonthlyTransactionDTO
import com.kevinroese.quartiermeister.mariadb.MonthlyTransaction
import com.kevinroese.quartiermeister.mariadb.repositories.MonthlyTransactionRepository
import org.springframework.stereotype.Service

@Service
class MonthlyTransactionService(
    val monthlyTransactionRepository: MonthlyTransactionRepository
) {
    fun listMonthlyTransactions(campaign: Long): List<MonthlyTransactionDTO> {
        return monthlyTransactionRepository.findAllByCampaign(campaign).map {
            MonthlyTransactionDTO(
                id = it.id,
                description = it.description,
                amount = it.amount
            )
        }
    }

    fun deleteMonthlyTransaction(transactionId: Long) {
        monthlyTransactionRepository.deleteById(transactionId)
    }

    fun saveMonthlyTransaction(monthlyTransactionDTO: MonthlyTransactionDTO) {
        monthlyTransactionRepository.save(
            MonthlyTransaction(
                id = monthlyTransactionDTO.id,
                description = monthlyTransactionDTO.description,
                amount = monthlyTransactionDTO.amount,
                campaign = monthlyTransactionDTO.campaign?: throw Exception("No campaign selected!")
            )
        )
    }
}