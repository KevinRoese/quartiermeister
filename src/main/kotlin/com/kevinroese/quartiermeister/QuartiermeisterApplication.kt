package com.kevinroese.quartiermeister

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class QuartiermeisterApplication

fun main(args: Array<String>) {
	runApplication<QuartiermeisterApplication>(*args)
}
