import { Crew } from "./crew";

export class Ship {
    id: number = 0;
    name: string = "";
    active: boolean = true;
    crew: Crew[] = [];
    campaign: number = 0;
}