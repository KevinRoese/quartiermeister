export class Rank {
    id: number = 0;
    name: string = "";
    salary: number = 0.0;
}