package com.kevinroese.quartiermeister.mariadb

import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table

@Entity
@Table(name = "campaigns")
data class Campaign(
    @Id
    val id: Long,
    val name: String,
    val status: Int = 0,
    val gold: Double = 0.0,
    val notes: String? = null
)
