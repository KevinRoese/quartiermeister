package com.kevinroese.quartiermeister.mariadb.repositories

import com.kevinroese.quartiermeister.mariadb.MonthlyTransaction
import com.kevinroese.quartiermeister.mariadb.Ship
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface MonthlyTransactionRepository: CrudRepository<MonthlyTransaction, Long> {
    fun findAllByCampaign(campaign: Long): List<MonthlyTransaction>

    @Query(value = "SELECT MAX(id) FROM monthly_transactions", nativeQuery = true)
    fun findMaxId(): Long?
}