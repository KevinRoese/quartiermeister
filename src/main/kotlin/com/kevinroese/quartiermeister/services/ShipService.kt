package com.kevinroese.quartiermeister.services

import com.kevinroese.quartiermeister.core.CrewDTO
import com.kevinroese.quartiermeister.core.RankDTO
import com.kevinroese.quartiermeister.core.ShipDTO
import com.kevinroese.quartiermeister.mariadb.Crewmember
import com.kevinroese.quartiermeister.mariadb.CrewmemberKey
import com.kevinroese.quartiermeister.mariadb.Rank
import com.kevinroese.quartiermeister.mariadb.Ship
import com.kevinroese.quartiermeister.mariadb.repositories.CrewmemberRepository
import com.kevinroese.quartiermeister.mariadb.repositories.RankRepository
import com.kevinroese.quartiermeister.mariadb.repositories.ShipRepository
import org.springframework.stereotype.Service

@Service
class ShipService(
    val shipRepository: ShipRepository,
    val crewmemberRepository: CrewmemberRepository,
    val rankRepository: RankRepository
) {
    fun listShips(campaignId: Long): List<ShipDTO> {
        return shipRepository.findAllByCampaign(campaignId).map {
            ShipDTO(
                id = it.id,
                name = it.name,
                active = it.active,
                crew = crewmemberRepository.findAllByCrewmemberKeyShip(it.id).map { crew ->
                    CrewDTO(
                        rank = crew.crewmemberKey.rank,
                        count = crew.count
                    )
                }
            )
        }
    }

    fun saveShip(shipDTO: ShipDTO) {
        shipRepository.save(
            Ship(
                id = shipDTO.id,
                name = shipDTO.name,
                active = shipDTO.active,
                campaign = shipDTO.campaign?: throw Exception("No campaign selected!")
            )
        )
        for(rank in rankRepository.findAll()) {
            crewmemberRepository.save(
                Crewmember(
                    crewmemberKey = CrewmemberKey(
                        rank = rank.id,
                        ship = shipDTO.id
                    ),
                    count = if(rank.id in shipDTO.crew.map { it.rank }) {
                        shipDTO.crew.map { it.rank to it.count }[rank.id.toInt()-1].second
                    } else 0
                )
            )
        }
    }

    fun deleteShip(shipId: Long) {
        shipRepository.deleteById(shipId)
        for (rank in rankRepository.findAll()) {
            crewmemberRepository.deleteById(CrewmemberKey(rank.id, shipId))
        }
    }

    fun listRanks(): List<RankDTO> {
        return rankRepository.findAll().map {
            RankDTO(
                id = it.id-1,
                name = it.name,
                salary = it.salary
            )
        }
    }

    fun saveRanks(ranks: List<RankDTO>) {
        rankRepository.saveAll(ranks.map {
            Rank(
                id = it.id,
                name = it.name,
                salary = it.salary
            )
        })
    }
}