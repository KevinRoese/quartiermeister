export class CommonItem {
    id: number = 0;
    campaign: number = 0;
    amount: number = 0;
    name: string = "";
}