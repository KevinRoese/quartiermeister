package com.kevinroese.quartiermeister.mariadb.repositories

import com.kevinroese.quartiermeister.mariadb.Crewmember
import com.kevinroese.quartiermeister.mariadb.CrewmemberKey
import com.kevinroese.quartiermeister.mariadb.Ship
import org.checkerframework.checker.units.qual.C
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface CrewmemberRepository: CrudRepository<Crewmember, CrewmemberKey> {
    fun findAllByCrewmemberKeyShip(ship: Long): List<Crewmember>

    @Query(value = "DELETE FROM crewmembers WHERE ship = ?1;", nativeQuery = true)
    fun deleteByCrewmemberKeyShip(ship: Long)
}