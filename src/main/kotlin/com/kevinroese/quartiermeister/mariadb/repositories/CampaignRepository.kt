package com.kevinroese.quartiermeister.mariadb.repositories

import com.kevinroese.quartiermeister.mariadb.Campaign
import com.kevinroese.quartiermeister.mariadb.Ship
import org.springframework.data.jdbc.repository.query.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface CampaignRepository: CrudRepository<Campaign, Long> {

    @Modifying
    @Query(value = "UPDATE campaigns SET status = :status WHERE id = :id", nativeQuery = true)
    fun saveStatus(id: Long?, status: Int): Long?
}