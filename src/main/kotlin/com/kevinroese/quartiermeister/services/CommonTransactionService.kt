package com.kevinroese.quartiermeister.services

import com.kevinroese.quartiermeister.core.CommonTransactionDTO
import com.kevinroese.quartiermeister.core.TransactionDTO
import com.kevinroese.quartiermeister.mariadb.CommonTransaction
import com.kevinroese.quartiermeister.mariadb.Transaction
import com.kevinroese.quartiermeister.mariadb.repositories.CommonTransactionRepository
import com.kevinroese.quartiermeister.mariadb.repositories.TransactionRepository
import org.checkerframework.checker.units.qual.C
import org.springframework.stereotype.Service

@Service
class CommonTransactionService(
    val commonTransactionRepository: CommonTransactionRepository
) {
    fun listTransactions(campaignId: Long): List<CommonTransactionDTO> {
        return commonTransactionRepository.findAllByCampaign(campaignId)
            .filter { it.parentTransaction == null }
            .map {
            CommonTransactionDTO(
                id = it.id,
                description = it.description?: "",
                amount = it.amount,
                subtransactions = commonTransactionRepository.findAllByParentTransaction(it.id).map { sub ->
                    CommonTransactionDTO(
                        id = sub.id,
                        description = sub.description ?:"",
                        amount = sub.amount,
                        subtransactions = listOf()
                    )
                }
            )
        }
    }

    fun deleteTransaction(transactionId: Long) {
        commonTransactionRepository.deleteById(transactionId)
    }

    fun saveTransaction(transaction: CommonTransactionDTO) {
        commonTransactionRepository.save(
            CommonTransaction(
                id = transaction.id,
                description = transaction.description,
                amount = transaction.amount,
                parentTransaction = null,
                campaign = transaction.campaign?: throw Exception("No campaign selected!")
            )
        )
        for (subTransaction in transaction.subtransactions?: listOf()) {
            commonTransactionRepository.save(
                CommonTransaction(
                    id = subTransaction.id,
                    description = subTransaction.description,
                    amount = subTransaction.amount,
                    parentTransaction = transaction.id,
                    campaign = transaction.campaign
                )
            )
        }
    }
}