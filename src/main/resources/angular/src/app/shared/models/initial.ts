import { Campaign } from "./campaign";
import { Rank } from "./rank";

export class Initial {
    campaigns: Campaign[] = [];
    ranks: Rank[] = [];
    lastTransaction: number = 0;
    lastMonthlyTransaction: number = 0;
    lastCommonTransaction: number = 0;
    lastShip: number = 0;
    lastCommonItem: number = 0;
}