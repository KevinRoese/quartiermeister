package com.kevinroese.quartiermeister.mariadb

import jakarta.persistence.*

@Entity
@Table(name = "crewmembers")
data class Crewmember(
    @EmbeddedId
    val crewmemberKey: CrewmemberKey,
    val count: Int = 0
)

@Embeddable
data class CrewmemberKey(
    val rank: Long,
    val ship: Long
)