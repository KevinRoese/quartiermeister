package com.kevinroese.quartiermeister.controllers

import com.kevinroese.quartiermeister.core.CommonItemDTO
import com.kevinroese.quartiermeister.core.RankDTO
import com.kevinroese.quartiermeister.core.ShipDTO
import com.kevinroese.quartiermeister.mariadb.CommonItem
import com.kevinroese.quartiermeister.services.CommonItemService
import com.kevinroese.quartiermeister.services.ShipService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
@CrossOrigin(maxAge = 72000)
class CommonItemController(
    val commonItemService: CommonItemService
) {
    @PostMapping("/items/saveSingle")
    fun saveShipSingle(@RequestBody commonItem: CommonItemDTO) {
        commonItemService.saveCommonItem(commonItem)
    }

    @GetMapping("/items/delete/{itemId}")
    fun deleteShip(@PathVariable itemId: Long) {
        commonItemService.deleteCommonItem(itemId)
    }
}