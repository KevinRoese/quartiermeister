export class CommonTransaction {
    id: number = 0;
    description: string = "";
    subtransactions: CommonTransaction[] = [];
    amount: number = 0.0;
    campaign: number = 0;

    expanded: boolean = false;
}