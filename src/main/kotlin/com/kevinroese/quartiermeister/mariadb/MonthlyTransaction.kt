package com.kevinroese.quartiermeister.mariadb

import jakarta.persistence.*

@Entity
@Table(name = "monthly_transactions")
data class MonthlyTransaction(
    @Id
    val id: Long,
    val campaign: Long,
    val description: String,
    val amount: Double
)
