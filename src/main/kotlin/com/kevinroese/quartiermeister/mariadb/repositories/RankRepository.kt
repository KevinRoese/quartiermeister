package com.kevinroese.quartiermeister.mariadb.repositories

import com.kevinroese.quartiermeister.mariadb.Rank
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface RankRepository: CrudRepository<Rank, Long> {
}