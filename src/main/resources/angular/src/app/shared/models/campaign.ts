import { CommonItem } from "./commonitem";
import { CommonTransaction } from "./commontransaction";
import { MonthlyTransaction } from "./monthlytransaction";
import { Ship } from "./ship";
import { Transaction } from "./transaction";

export class Campaign {
    id: number = 0;
    name: string = "";
    status: number = 0;
    gold: number = 0;
    notes: string = "";
    transactions: Transaction[] = [];
    transactions_executed: Transaction[] = [];
    monthly_transactions: MonthlyTransaction[] = [];
    common_transactions: CommonTransaction[] = [];
    ships: Ship[]= [];
    common_items: CommonItem[] = [];
}