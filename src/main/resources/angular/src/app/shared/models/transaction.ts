export class Transaction {
    id: number = 0;
    description: string = "";
    executed: boolean = false;
    subtransactions: Transaction[] = [];
    amount: number = 0.0;
    multiplier: number = 1;
    campaign: number = 0;

    expanded: boolean = false;
}