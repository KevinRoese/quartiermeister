package com.kevinroese.quartiermeister.mariadb

import jakarta.persistence.*

@Entity
@Table(name = "common_transactions")
data class CommonTransaction(
    @Id
    val id: Long,
    val campaign: Long,
    val description: String? = null,
    @Column(name = "parent_transaction")
    val parentTransaction: Long? = null,
    val amount: Double
)
