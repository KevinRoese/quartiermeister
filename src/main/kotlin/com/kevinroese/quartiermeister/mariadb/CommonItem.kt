package com.kevinroese.quartiermeister.mariadb

import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table

@Entity
@Table(name = "common_items")
data class CommonItem(
    @Id
    val id: Long,
    val campaign: Long,
    val amount: Int,
    val name: String
)
