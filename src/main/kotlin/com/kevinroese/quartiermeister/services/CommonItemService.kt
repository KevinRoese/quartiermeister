package com.kevinroese.quartiermeister.services

import com.kevinroese.quartiermeister.core.CommonItemDTO
import com.kevinroese.quartiermeister.core.CrewDTO
import com.kevinroese.quartiermeister.core.RankDTO
import com.kevinroese.quartiermeister.core.ShipDTO
import com.kevinroese.quartiermeister.mariadb.*
import com.kevinroese.quartiermeister.mariadb.repositories.CommonItemRepository
import com.kevinroese.quartiermeister.mariadb.repositories.CrewmemberRepository
import com.kevinroese.quartiermeister.mariadb.repositories.RankRepository
import com.kevinroese.quartiermeister.mariadb.repositories.ShipRepository
import org.springframework.stereotype.Service

@Service
class CommonItemService(
    val commonItemRepository: CommonItemRepository
) {
    fun listCommonItems(campaignId: Long): List<CommonItemDTO> {
        return commonItemRepository.findAllByCampaign(campaignId).map {
            CommonItemDTO(
                id = it.id,
                name = it.name,
                amount = it.amount
            )
        }
    }

    fun saveCommonItem(commonItemDTO: CommonItemDTO) {
        commonItemRepository.save(
            CommonItem(
                id = commonItemDTO.id,
                name = commonItemDTO.name,
                amount = commonItemDTO.amount,
                campaign = commonItemDTO.campaign?: throw Exception("No campaign selected!")
            )
        )
    }

    fun deleteCommonItem(commonItemId: Long) {
        commonItemRepository.deleteById(commonItemId)
    }
}