package com.kevinroese.quartiermeister.controllers

import com.kevinroese.quartiermeister.core.RankDTO
import com.kevinroese.quartiermeister.core.ShipDTO
import com.kevinroese.quartiermeister.services.ShipService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
@CrossOrigin(maxAge = 72000)
class ShipController(
    val shipService: ShipService
) {
    @PostMapping("/ships/saveSingle")
    fun saveShipSingle(@RequestBody ship: ShipDTO) {
        shipService.saveShip(ship)
    }

    @GetMapping("/ships/delete/{shipId}")
    fun deleteShip(@PathVariable shipId: Long) {
        shipService.deleteShip(shipId)
    }

    @GetMapping("/ranks/list")
    fun getRanks(): List<RankDTO> {
        return shipService.listRanks()
    }

    @PostMapping("/ranks/save")
    fun saveRanks(@RequestBody ranks: List<RankDTO>) {
        shipService.saveRanks(ranks)
    }
}