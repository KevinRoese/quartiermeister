package com.kevinroese.quartiermeister.mariadb.repositories

import com.kevinroese.quartiermeister.mariadb.Ship
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ShipRepository: CrudRepository<Ship, Long> {
    fun findAllByCampaign(campaign: Long): List<Ship>

    @Query(value = "SELECT MAX(id) FROM ships", nativeQuery = true)
    fun findMaxId(): Long?
}